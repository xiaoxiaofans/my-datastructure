package com.zbxx.leetcode.tree;

import com.zbxx.leetcode.struct.ListNode;
import com.zbxx.leetcode.struct.TreeNode;


/**
 * 1367 二叉树中的连标
 *
 * @author wanrj
 * @date 2023/6/5
 */
public class isSubPath {

    public boolean isSubPath(ListNode head, TreeNode root) {
        boolean currentMatch = match(head, root);
        return currentMatch || root != null && (isSubPath(head, root.left) || isSubPath(head, root.right));
    }

    public boolean match(ListNode lnode, TreeNode node) {
        if (lnode == null && node == null) {
            return true;
        }
        if (lnode == null) {
            return true;
        }
        if (node == null) {
            return false;
        }
        if (lnode.val != node.val) {
            return false;
        }
        return match(lnode.next, node.left) || match(lnode.next, node.right);
    }

}
