package com.zbxx.leetcode.tree;

import com.zbxx.leetcode.struct.TreeNode;

import java.util.Deque;
import java.util.LinkedList;


/**
 * 513找树左下角的值
 *
 * @author wanrj
 * @date 2023/6/6
 */
public class findBottomLeftValue {

    public int findBottomLeftValue(TreeNode root) {
        Deque<TreeNode> deque = new LinkedList<>();
        deque.add(root);
        deque.add(null);
        TreeNode res = root;
        while (!deque.isEmpty()) {
            TreeNode n = deque.poll();
            if (n == null) {
                res = deque.peek() != null ? deque.peek() : res;
                continue;
            }
            if (n.left != null) {
                deque.offer(n.left);
            }
            if (n.right != null) {
                deque.offer(n.right);
            }
            if (deque.peek() == null) {
                deque.offer(null);
            }
        }
        return res.val;
    }

}
