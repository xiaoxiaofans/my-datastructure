package com.zbxx.leetcode.kt

/**
 * 917 仅仅翻转字母
 */
object reverseOnlyLetters {
    fun reverseOnlyLetters(s: String): String {
        val chr = s.toCharArray();
        var l = 0;
        var r = chr.size - 1
        while (l < r) {
            while (l < r && !chr[l].isLetter()) {
                l++
            }
            while (l < r && !chr[r].isLetter()) {
                r--
            }
            if (l < r) {
                swap(chr, l, r)
                l++
                r--
            }
        }
        return String(chr)
    }

    fun swap(chr: CharArray, a: Int, b: Int) {
        val c = chr[a]
        chr[a] = chr[b]
        chr[b] = c
    }
}


fun main(args: Array<String>) {
    val s = reverseOnlyLetters.reverseOnlyLetters("a-bC-dEf-ghIj")
    println(s)
}