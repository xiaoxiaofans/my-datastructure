package com.zbxx.leetcode.practice.array;


/**
 * @author wanrj
 * @date 2023/1/6
 * @description 2180 统计各位数字之和
 */
public class countEven {


    public static void main(String[] args) {
        System.out.println(new countEven().countEven(30));
    }

    public int countEven(int num) {
        int res = 0;
        for (int i = 2; i < num; i++) {
            res += String.valueOf(i).chars().map(s -> s - '0').reduce(0, Integer::sum) % 2 == 0 ? 1 : 0;
        }
        return res;
    }


}
