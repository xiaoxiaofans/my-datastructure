package com.zbxx.leetcode.tree;

import com.zbxx.leetcode.struct.TreeNode;

import java.util.ArrayList;
import java.util.List;

/**
 * 199 二叉树的右视图
 *
 * @author wanrj
 * @date 2023/6/7
 */
public class rightSideView {


    public List<Integer> rightSideView(TreeNode root) {
        List<Integer> ans = new ArrayList<>();
        dfs(ans, root, 1, new TreeNode(1));
        return ans;
    }

    public void dfs(List<Integer> ans, TreeNode root, int depth, TreeNode d) {
        if (root == null) {
            return;
        }
        if (depth == d.val) {
            ans.add(root.val);
            d.val++;
        }
        dfs(ans, root.right, depth + 1, d);
        dfs(ans, root.left, depth + 1, d);
    }


}
